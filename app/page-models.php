<?php
require_once('header.php');
get_header('page-models');
?>

  <div class="layout-outer">

    <!-- The Banner Background -->
    <div class="banner-bg-wrap">
      <div class="banner-bg"></div>
    </div>

    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Models</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title-2">Models</h1>

      <!-- The Tab -->
      <ul class="ts-tabs">
        <li class="tab-item active"><a data-content-selector="#tab-content-1"><span>Custom Bobber</span></a></li>
        <li class="tab-item "><a data-content-selector="#tab-content-2"><span>NEO SPORTS CAFE'</span></a></li>
<!--        <li class="tab-item "><a data-content-selector="#tab-content-2"><span>TOURING</span></a></li>-->
<!--        <li class="tab-item "><a data-content-selector="#tab-content-2"><span>Uncategorized</span></a></li>-->
<!--        <li class="tab-item "><a data-content-selector="#tab-content-2"><span>รถจักรยานยนต์แบบปีนเขาวิบาก</span></a></li>-->
      </ul>

      <!-- Content for tab 1 -->
      <div class="tab-content" id="tab-content-1">
        <!-- Buttons -->
        <ul class="btn-toggle-container">
          <li class="btn-toggle active"><a data-content-selector=".category-content-1"><span>CB World</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-3"><span>Family</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-2"><span>A.T.</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-2"><span>500 Series</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-2"><span>600 Series</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-2"><span>A.T.</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-4"><span>Sport</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-6"><span>On/Off Road</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-5"><span>Bobber</span></a></li>
        </ul>

        <!-- Grid -->
        <?php for ($h = 0; $h < 6; $h++): ?>
          <div class="card-grid category-content category-content-<?php echo $h + 1 ?>">
            <?php for ($i = 0; $i < $h+2; $i++): ?>
              <a href="#" class="product-card card-col">
                <div class="_inner">
                  <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
                  <div class="inside-label">C125</div>
                </div>
              </a>
            <?php endfor; ?>
          </div>
        <?php endfor; ?>
      </div>

      <!-- Content for tab 1 -->
      <div class="tab-content" id="tab-content-2">
        <!-- Buttons -->
        <ul class="btn-toggle-container">
          <li class="btn-toggle active"><a data-content-selector=".category-content-1"><span>CB World</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-3"><span>Family</span></a></li>
          <li class="btn-toggle"><a data-content-selector=".category-content-2"><span>A.T.</span></a></li>
        </ul>

        <!-- Grid -->
        <?php for ($h = 0; $h < 3; $h++): ?>
          <div class="card-grid category-content category-content-<?php echo $h + 1 ?>">
            <?php for ($i = 0; $i < $h+6; $i++): ?>
              <a href="#" class="product-card card-col">
                <div class="_inner">
                  <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-2.jpg' ?>"/>
                  <div class="inside-label">C125</div>
                </div>
              </a>
            <?php endfor; ?>
          </div>
        <?php endfor; ?>
      </div>

    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
