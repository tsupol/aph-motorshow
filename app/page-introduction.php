<?php
require_once('header.php');
get_header('page-introduction'); // using the same body's class as Wordpress' post
?>
<div class="page-container">
  <div id="sec-banner" class="layout-outer theme-dark">

    <!-- The Banner Background -->
    <div class="banner-bg-wrap">
      <div class="banner-bg"></div>
    </div>

    <div class="layout-inner">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">An Introduction</a></span>
      </div>

      <div class="banner-text">
        <h1 class="banner-title-1 color-1">
          An Introduction
        </h1>
        <h2 class="banner-title-2">
          <span>Motor Show <b class="color-1">2019</b></span>
        </h2>
      </div>

      <!--    <svg class="line-1" width="100%" height="100%">-->
      <!--      <path fill="transparent" stroke="#000000" stroke-dasharray="4 1" stroke-width="2" d="M10 80 Q 77.5 10, 145 80 T 280 80" class="path"></path>-->
      <!--    </svg>-->
    </div>
  </div>

  <div id="sec-aphonda" class="layout-outer theme-dark">
    <div class="layout-inner page-wrapper">
      <img class="dashed-line" id="line-1" src="<?php echo get_template_directory_uri() . 'img/about/line1.svg' ?>"/>
      <div class="title-container">
        <h2 class="title-2 color-primary">A.P. Honda BOOTH</h2>
        <h3 class="title-3">WING of RACE</h3>
      </div>
      <img class="section-bg show-sm" src="<?php echo get_template_directory_uri() . 'img/about/about-bg-aphonda-mobile.jpg' ?>">
      <div class="float-right">
        <div class="section-content">
          <p class="section-desc">
            “<br/>
            ปีกคืออัตลักษณ์ของความเป็น Honda และยังสื่อถึง<br/>
            ความโฉบเฉี่ยว แสดงให้เห็นถึงความเป็นสปอร์ตที่เป็นวาระ<br/>
            ร่วมกันของ Honda ไม่ว่าจะเป็น BigWing หรือ Wing<br/>
            Center ที่จะยกระดับความเป็นสปอร์ตไปพร้อมๆกัน<br/>
            <span class="bottom-quote">”</span>
          </p>
          <div class="btn-wrap">
            <a href="#" class="ts-btn btn-primary">
              <span>ตารางงาน A.P. Honda</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Section CUB -->
  <div id="sec-cub" class="layout-outer theme-cub">
    <div class="layout-inner">
      <img class="dashed-line" id="line-2" src="<?php echo get_template_directory_uri() . 'img/about/line2.svg' ?>"/>
      <div class="title-container">
        <h2 class="title-2">CUB House BOOTH</h2>
        <h3 class="title-3 color-primary">Open House</h3>
      </div>
      <img class="section-bg show-sm" src="<?php echo get_template_directory_uri() . 'img/about/about-bg-cub-mobile.jpg' ?>">
      <div class="float-left">
        <div class="section-content">
          <p class="section-desc">
            “<br/>
            เชื้อเชิญให้ผู้คนได้มาสัมผัสกับบริบทใหม่ของ CUB House<br/>
            กับโลกของ DIY<br/>
            <span class="bottom-quote">”</span>
          </p>

          <div class="btn-wrap">
            <a href="#" class="ts-btn btn-primary">
              <span>ตารางงาน CUB House</span>
            </a>
          </div>
        </div>
      </div>

      <img class="dashed-line" id="line-3" src="<?php echo get_template_directory_uri() . 'img/about/line3.svg' ?>"/>
      <img id="what-stop-you" src="<?php echo get_template_directory_uri() . 'img/about/what-stop-you.jpg' ?>"/>

      <div class="desc-2">
        “<br/>
        ท้าทายคนรุ่นใหม่ ให้กล้าออกไป<br class="show-sm"/> ใช้ชีวิตตามไลฟ์สไตล์ของตัวเอง<br class="hide-sm"/>
        และ <br class="show-sm"/><b class="color-1">Honda</b> จะเป็นส่วนเติมเต็มไลฟ์สไตล์<br class="show-sm"/>การใช้ชีวิตของทุกคน<br/>
        <span class="bottom-quote">”</span>
      </div>
    </div>
  </div>

  <!-- Section CUB -->
  <div id="sec-location" class="layout-outer theme-cub">
    <div class="layout-inner">
      <img class="dashed-line" id="line-4" src="<?php echo get_template_directory_uri() . 'img/about/line4.svg' ?>"/>
      <div class="_section">
        <div>
          <h2 class="title-1">Location</h2>
          <p class="location-desc">
            IMPACT เมืองทองธานี<br class="hide-sm">
            อาคารชาลเลนเจอร์ 1-3
          </p>
        </div>
        <div class="map-wrap">
          <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3872.772048204098!2d100.54547001419952!3d13.912569090243561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2830b0b06eef3%3A0x5ade5eb90113ca18!2sIMPACT+Arena%2C+Exhibition+and+Convention+Center%2C+Muang+Thong+Thani!5e0!3m2!1sen!2sth!4v1553353241273"
              width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="_section">
        <p class="booth-desc">บูท : MC7 / MC 16 / MC17</p>
        <img class="booth-location" src="<?php echo get_template_directory_uri() . 'img/about/booth-location.jpg' ?>"/>
      </div>
    </div>
  </div>
</div>
<?php include_once('footer.php') ?>
