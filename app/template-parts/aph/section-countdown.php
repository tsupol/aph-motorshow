<div class="layout-full-width theme-dark">
  <div class="full-height-container">

    <div class="honda-logo-space"></div>

    <div class="flip-clock-container">
      <div class="flip-clock-outer">
        <div class="flip-clock-inner">
          <div id="flip-clock"></div>
        </div>
      </div>
    </div>

    <div class="page-bottom">
      <div class="text-wrap">
        <div class="_text-pre-title">เตรียมพบกับ</div>
        <h1 class="_title-main">
          <span>A.P. Honda IN</span><br class="show-md">
          <span>&nbsp;Motor Show <b class="color-1">2019</b></span>
        </h1>
        <div class="_text-subtitle">งานที่คนรักมอเตอร์ไซค์ฮอนด้าต้องไม่พลาด!</div>
        <div class="_text-date">วันที่ 27 มีนาคม - 7 เมษายน นี้</div>
        <div class="_padding-bottom"></div>
      </div>
    </div>

  </div>
</div>
