<li class="menu-item current-menu-item"><a href="..">Home</a></li>
<li class="menu-item"><a href="./page-introduction.php">An Introduction</a></li>
<li class="menu-item menu-item-has-children">
  <a>Honda Bike</a>
  <ul class="sub-menu">
    <li class="menu-item"><a href="#">Models</a></li>
    <li class="menu-item"><a href="#">Promotion</a></li>
    <li class="menu-item"><a href="#">Collection</a></li>
    <li class="menu-item"><a href="#">Brochures</a></li>
    <li class="menu-item"><a href="#">News & Video</a></li>
    <li class="menu-item"><a href="#">Time Schedule</a></li>
    <li class="menu-item"><a href="#">Race Queen</a></li>
  </ul>
</li>
<li class="menu-item menu-item-has-children">
  <a>CUB House</a>
  <ul class="sub-menu">
    <li class="menu-item"><a href="#">Promotion</a></li>
    <li class="menu-item"><a href="#">Lifestyle Collection</a></li>
    <li class="menu-item"><a href="#">Accessories</a></li>
    <li class="menu-item"><a href="#">Brochures</a></li>
    <li class="menu-item"><a href="#">News & Video</a></li>
    <li class="menu-item"><a href="#">Time Schedule</a></li>
    <li class="menu-item"><a href="#">Motor Stylist Girl</a></li>
  </ul>
</li>
