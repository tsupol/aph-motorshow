<?php
$asset_path = get_template_directory_uri();
?>
<div class="menu-social-wrap">
  <ul class="menu-social">
    <li class="menu-label">Follow Us</li>
    <li class="menu-icon">
      <a href="#">
        <!-- Has 2 icons just for Internet Explorer. -->
        <img class="menu-icon-img hide-nav" src="<?php echo $asset_path . '/img/icon-facebook.png' ?>"/>
        <img class="menu-icon-img show-nav" src="<?php echo $asset_path . '/img/icon-facebook-invert.png' ?>"/>
      </a>
    </li>
    <li class="menu-icon">
      <a href="#">
        <img class="menu-icon-img hide-nav" src="<?php echo $asset_path . '/img/icon-line.png' ?>"/>
        <img class="menu-icon-img show-nav" src="<?php echo $asset_path . '/img/icon-line-invert.png' ?>"/>
      </a>
    </li>
    <li class="menu-icon">
      <a href="#">
        <img class="menu-icon-img hide-nav" src="<?php echo $asset_path . '/img/icon-youtube.png' ?>"/>
        <img class="menu-icon-img show-nav" src="<?php echo $asset_path . '/img/icon-youtube-invert.png' ?>"/>
      </a>
    </li>
    <li class="menu-icon">
      <a href="#">
        <img class="menu-icon-img hide-nav" src="<?php echo $asset_path . '/img/icon-twitter.png' ?>"/>
        <img class="menu-icon-img show-nav" src="<?php echo $asset_path . '/img/icon-twitter-invert.png' ?>"/>
      </a>
    </li>
  </ul>
</div>
