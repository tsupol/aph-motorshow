<?php
require_once('header.php');
get_header('page-brochures');
?>

  <div class="layout-outer">

    <!-- The Banner Background -->
    <div class="banner-bg-wrap">
      <div class="banner-bg"></div>
    </div>

    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Brochures</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title">Brochures</h1>

      <!-- The Sub Title -->
      <h2 class="sub-title">Honda BigBike</h2>

      <!-- Grid -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 6; $i++): ?>
          <a href="#" class="product-card card-col">
            <div class="_inner">
              <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
              <div class="inside-label">หมวกกันน็อกเบลล์ คัทตอม500</div>
            </div>
            <div class="product-desc">
              PDF 3.5 MB
            </div>
          </a>
        <?php endfor; ?>
      </div>

      <!-- Category 2-->

      <hr class="section-divider" />

      <!-- The Sub Title -->
      <h2 class="sub-title color-primary">Honda Motorcycle</h2>

      <!-- Grid -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 6; $i++): ?>
          <a href="#" class="product-card card-col">
            <div class="_inner">
              <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
              <div class="inside-label">หมวกกันน็อกเบลล์ คัทตอม500</div>
            </div>
            <div class="product-desc">
              PDF 3.5 MB
            </div>
          </a>
        <?php endfor; ?>
      </div>


    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
