<?php
require_once('header.php');
get_header('page-race-queen');
?>

  <div class="layout-outer">

    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Race Queen</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title">Race Queen</h1>

      <!-- Grid -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 4; $i++): ?>
          <div class="card-col">
            <a href="#" class="person-card">
              <div class="person-thumb-wrap">
                <img class="person-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/race-queen-1.jpg' ?>"/>
              </div>
              <div class="person-title">ภัชรพร ชวนะกุล</div>
              <div class="person-subtitle">น้องเจ็ม</div>
            </a>
          </div>
        <?php endfor; ?>
      </div>

      <hr class="section-divider"/>

      <!-- The Sub Title -->
      <h2 class="sub-title-2">Gallery</h2>

      <!-- Grid (Masonry) -->
      <div class="gallery-container" itemscope itemtype="http://schema.org/ImageGallery">
        <?php

        $imgData = array(
          array(
            'large_img' => get_template_directory_uri() . 'img/gallery/gallery-large-1.jpg',
            'thumb'     => get_template_directory_uri() . 'img/gallery/gallery-small-1.jpg',
            'size'      => '570x425', // must provide the correct size for large image especially for mobile device pinching zoom.
          ),
          array(
            'large_img' => get_template_directory_uri() . 'img/gallery/gallery-large-2.jpg',
            'thumb'     => get_template_directory_uri() . 'img/gallery/gallery-small-2.jpg',
            'size'      => '867x1164',
          ),
          array(
            'large_img' => get_template_directory_uri() . 'img/gallery/gallery-large-1.jpg',
            'thumb'     => get_template_directory_uri() . 'img/gallery/gallery-small-1.jpg',
            'size'      => '570x425',
          ),
          array(
            'large_img' => get_template_directory_uri() . 'img/gallery/gallery-large-1.jpg',
            'thumb'     => get_template_directory_uri() . 'img/gallery/gallery-small-1.jpg',
            'size'      => '570x425',
          ),
        );

        for ($i = 0; $i < 3; $i++):
          foreach ($imgData as $img): ?>
            <figure class="gallery-item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
              <!-- Large image: please provide the path and the size to 'large' version of the image -->
              <a href="<?php echo $img['large_img'] ?>" itemprop="contentUrl" data-size="<?php echo $img['size'] ?>">
                <!-- Small image: small image just for thumbnail and save the page load time -->
                <div class="gallery-thumb-wrap">
                  <img class="gallery-thumb" src="<?php echo $img['thumb'] ?>" itemprop="thumbnail" alt="Image description"/>
                </div>
              </a>
              <figcaption itemprop="caption description">
                เหล่าพริตตี้ในงาน Honda Motor Show 2019 โชว์ Shot ว้าว วัดกันภาพต่อภาพ
              </figcaption>
            </figure>
          <?php
          endforeach;
        endfor;
        ?>
      </div>


    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
