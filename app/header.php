<?php
$asset_path = "./";

require_once('dev-inc/mimic-wordpress.php');

function get_header($page_name = '') {
global $asset_path
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  <meta name="description" content="SCG International, Expertise in complete services and solutions with international presences and strong alliances.">
  <title>APHonda | Motor Show</title>
  <link rel="canonical" href="http://aph-motorshow.artplore.com"/>
  <link rel="stylesheet" href="fonts/db_helvethaicax/stylesheet.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="fonts/ap-honda/stylesheet.css" type="text/css" media="all"/>
  <link href="https://fonts.googleapis.com/css?family=Oswald:500" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Prompt:500" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/default-skin/default-skin.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" type="text/css" media="all"
        href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
  <meta name="robots" content="noindex"/>
</head>
<body class="<?php echo $page_name ?>">

<div class="layout-main">
  <div id="footer-bottom-push">
<?php
include_once('./nav.php');
include_once('./photo-viewer.php');
} // end get_header()
?>
