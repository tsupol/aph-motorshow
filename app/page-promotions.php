<?php
require_once('header.php');
get_header('page-promotions');
?>

  <div class="layout-outer">

    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Promotions</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title">Promotions</h1>

      <!-- The Sub Title -->
      <h2 class="sub-title">รวมโปรโมชัน</h2>

      <!-- Grid -->
      <div class="card-grid grid-50">
        <?php for ($i = 0; $i < 3; $i++): ?>
          <div class="card-col">
            <a href="#" class="promotion-card">
              <img class="promotion-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/promotion-1.jpg' ?>"/>
            </a>
          </div>
        <?php endfor; ?>
      </div>

      <hr class="section-divider"/>

      <!-- The Sub Title -->
      <h2 class="sub-title">Honda BigBike</h2>

      <!-- Grid -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 6; $i++): ?>
          <a href="#" class="product-card card-col">
            <div class="_inner">
              <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
              <div class="inside-label">หมวกกันน็อกเบลล์ คัทตอม500</div>
            </div>
          </a>
        <?php endfor; ?>
      </div>

      <hr class="section-divider"/>

      <!-- The Sub Title -->
      <h2 class="sub-title color-primary">Honda Motorcycle</h2>

      <!-- Grid -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 1; $i++): ?>
          <a href="#" class="product-card card-col">
            <div class="_inner">
              <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
              <div class="inside-label">หมวกกันน็อกเบลล์ คัทตอม500</div>
            </div>
          </a>
        <?php endfor; ?>
      </div>


    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
