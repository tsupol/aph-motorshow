<!-- For Pre-launch only -->
<div class="nav-logo-honda">
  <?php the_custom_logo() ?>
</div>

<div id="ts-backdrop"></div>
<nav id="site-navigation" class="main-navigation">
  <div class="nav-inner">
    <div class="nav-logo-wrap">
      <?php the_custom_logo() ?>
    </div>
    <div id="nav-toggle">
      <!-- stripe -->
      <span></span>
    </div>

    <!-- WP
     wp_nav_menu(array(
          'container_class' => 'menu-content-wrap',
          'theme_location'  => 'menu-1',
          'menu_id'         => 'primary-menu',
        ));
    -->
    <!-- This wrapper is necessary for displaying the 'mobile menu drop-down' -->
    <div class="menu-content-wrap">
      <ul id="primary-menu">
        <?php get_template_part('template-parts/aph/menu-content-primary') ?>
      </ul>
      <!-- Social -->
      <?php get_template_part('template-parts/aph/menu-content-social') ?>
    </div>
  </div>
</nav>
