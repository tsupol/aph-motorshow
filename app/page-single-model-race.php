<?php
require_once('header.php');
get_header('page-single-model');
?>
  <main id="main" class="site-main">
    <div class="layout-outer">
      <div class="layout-inner page-wrapper">
        <article class="post type-post has-post-thumbnail">

          <header class="entry-header">
            <div class="entry-crumbs">
              <span><a class="entry-crumb" href="#">Home</a></span>
              <span class="_gt">&gt;</span>
              <span><a class="entry-crumb" href="#">Honda Bike</a></span>
              <span class="_gt">&gt;</span>
              <span><a class="entry-crumb" href="#">World ‘s Number 1 Racing Machine</a></span>
            </div>

            <div class="entry-title-wrap">
              <h1 class="entry-title title-full-width">World ‘s Number 1 Racing Machine</h1>
            </div>

            <!-- Share, move to content -->
            <div class="entry-shares pinned-share">
              <div class="_label">Share on :</div>
              <a href="#"><img class="_icon" src="<?php echo $asset_path . '/img/icon-facebook-invert.png' ?>"/></a>
              <a href="#"><img class="_icon" src="<?php echo $asset_path . '/img/icon-line-invert.png' ?>"/></a>
              <a href="#"><img class="_icon" src="<?php echo $asset_path . '/img/icon-twitter-invert.png' ?>"/></a>
            </div>

            <div class="entry-meta"></div>

          </header><!-- .entry-header -->

          <div class="entry-content">

            <!-- Banner -->
            <div class="full-width-section">
              <div class="_banner-img-wrap">
                <img class="_banner-img" src="<?php echo get_template_directory_uri() . 'img/placeholder/banner-single-model.jpg' ?>"/>
              </div>
              <div class="banner-text">
                <h2>
                  Honda racing in<br/>
                  MotoGP<span class="sup">™</span>
                </h2>
                <p class="bigger">รถแข่งของ Marc Márquez ในการแข่งขัน MotoGP™</p>
              </div>
            </div>

            <!-- The container that the Social Share attached to -->
            <div class="actual-content">

              <!-- Award Detail -->
              <p class="award-detail">
                สรุปประวัติความสำเร็จของนักแข่ง Repsol Honda Team ชุดนี้กันอีกครั้ง สำหรับ “เด็กระเบิด” Marc Márquez เจ้าของตำแหน่งแชมป์โลกการแข่งขัน MotoGP™ ในปีล่าสุด
                จากความชื่นชอบมอเตอร์ไซค์ในวัยเด็ก สู่การเป็นนักแข่งระดับอาชีพในวัยเพียง 15 ปี โดยหลังจากเข้าร่วมการแข่งขันระดับอาชีพได้ไม่นาน
                เจ้าตัวก็ฉายแววซุปเปอร์สตาร์ได้ทันทีด้วยการคว้าแชมป์โลกรุ่น
                125 cc ในปี 2010 และก้าวสู่รายการแข่งขัน Moto2™ คว้าแชมป์โลกได้สำเร็จในปี 2012 ด้วยความสามารถ และความพยายามอย่างไม่ย่อท้อ ส่งต่อเป็นแรงผลักดันให้ Marc Márquez
                ก้าวสู่การเป็นนักแข่งที่ฉายแสงในวงการ Motorsport ได้อย่างเต็มภาคภูมิ โดยเป็นนักแข่งคนแรกในรอบ 69 ปี ของประวัติศาสตร์ Grand Prix Motorcycle Racing ที่สามารถชนะ และคว้าแชมป์โลกรุ่น
                MotoGP™
                ได้มากถึง 5 ครั้ง ภายในระยะเวลา 8 ปี ของอาชีพนักแข่งของเขา
              </p>

              <!-- Award Info -->
              <div class="model-info-container award-container">
                <h2 class="model-info-title">
                  <span>รางวัลที่ได้</span>
                </h2>

                <div class="awards">

                  <div class="model-info-wrap">
                    <div class="model-info">
                      <h3 class="model-info-label">ปีที่เป็นแชมป์ :</h3>
                      <p>2013 / 2014 / 2016 / 2017 / 2018</p>
                    </div>
                  </div>

                  <div class="model-info-wrap">
                    <div class="model-info">
                      <h3 class="model-info-label">รายการแข่งขัน :</h3>
                      <p>MotoGP</p>
                    </div>
                  </div>

                  <div class="model-info-wrap">
                    <div class="model-info">
                      <h3 class="model-info-label">ทีมที่แข่ง:</h3>
                      <p>Repsol Honda Team</p>
                    </div>
                  </div>

                  <div class="model-info-wrap">
                    <div class="model-info">
                      <h3 class="model-info-label">รุ่นรถที่ใช้ในการแข่งขัน</h3>
                      <p>Honda RC213V</p>
                    </div>
                  </div>


                </div>
              </div>

              <!-- Model Content -->
              <div class="model-content">
                <div class="model-view">
                  <div class="model-img-wrap race-model">
                    <img class="model-img active" id="model-1" src="<?php echo get_template_directory_uri() . 'img/placeholder/model-1.jpg' ?>"/>
                  </div>
                </div> <!-- .model-view -->

                <!-- Model Info -->
                <div class="model-info-container">
                  <h2 class="model-info-title">
                    <span>ข้อมูลผลิตภัณฑ์</span>
                  </h2>

                  <div>
                    <div class="model-info">
                      <h3 class="model-info-label">เครื่องยนต์ :</h3>
                      <p>SC77E/Liquid-cooled 4-stroke 16-valve DOHC Inline-41,000</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ปริมาตรกระบอกสูบ (ซีซี) :</h3>
                      <p>1,000</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ความกว้างกระบอกสูบ (มม.) X ช่วงชัก (มม.) :</h3>
                      <p>76 x 55</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">อัตราส่วนแรงอัด :</h3>
                      <p>13.0:1</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบคลัทช์ :</h3>
                      <p>Wet, multiplate with diaphragm spring with assist slipper</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบส่งกำลัง :</h3>
                      <p>6-Speed manual</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบเฟืองท้าย :</h3>
                      <p>chain</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">อัตราทดรอบ/เส้นรอบวงล้อ :</h3>
                      <p>2.688</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบเกียร์ :</h3>
                      <p>1st gear 2.285 2nd gear 1.777 3rd gear 1.500 4th gear 1.333 5th gear 1.214 6th gear 1.137</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบจุดระเบิด :</h3>
                      <p>Digital transistorized with electronic advance</p>
                    </div>
                  </div>

                  <div class="bottom-btn-wrap">
                    <div class="_btn-share-wrap">
                      <div class="ts-btn _btn-share"></div>
                    </div>
                  </div>


                </div> <!-- .model-info-container -->
              </div> <!-- .model-content -->


            </div> <!-- .actual-content -->
          </div>
        </article>

      </div><!-- .layout-inner -->
    </div><!-- .layout-outer -->
  </main>
<?php
include_once('footer.php');
