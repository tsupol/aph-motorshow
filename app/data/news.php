<?php
$data = array();
ob_start(); ?>
  <p class="_emphasis">
    SCG Trading Co., Ltd., a subsidiary of SCG, has recently signed an MOU with Ralali PTE. LTD., the owner of www.ralali.com - one of the leading digital market platforms in Indonesia, on “B2B market platform” project.   This collaboration is aimed to explore possibility to develop a B2B market platform business in ASEAN.
  </p>
  <p>
    Value-added services from SCG Trading and online business expertise from Ralali will enable sellers to offer their products to customers through the online platform and the platform can offer certain services for online store management and orders fulfillment such as logistics service to the sellers. This is another step of SCG Trading to capture the changing customers’ behaviors and increasing demands of online business in Thailand and ASEAN.
  </p>
  <p>In the Picture (from left)</p>
  <ul>
    <li>Mr. Nopporn Latthitham, Digital Transformation – Head, SCG Trading Co., Ltd.</li>
    <li>Mr. Alexander Lukman, Chief Operating Officer, Ralali PTE. LTD.</li>
    <li>Mr. Kitti Pattanaleenakul, ASEAN Business Director of SCG Trading Co., Ltd.</li>
    <li>Mr. Bunn Kasemsup, Managing Director, SCG Trading Co., Ltd.</li>
    <li>Mr. Joseph Aditya, CEO, Ralali PTE. LTD.</li>
    <li>Mr. Nithi Patarachoke, President, Cement Building Materials Business of SCG</li>
    <li>Mr. Yuttana Jiamtragan, Vice President-Corporate Administration, SCG</li>
    <li>Mr. Chandra Tandiono, Head of Growth and Expansion, Ralali PTE. LTD.</li>
  </ul>
<?php $data[] = array(
  "title"     => "SCG TEAMS UP WITH RALALI.COM TO EXPLORE B2B MARKET PLATFORM OPPORTUNITY IN ASEAN",
  "category"  => "activity",
  "posted_at" => "2017-12-19 14:00:07",
  "content"   => ob_get_clean(),
  "thumb"     => "./img/news/news-1.jpg",
  "images"    => [
    "./img/news/news-1.jpg",
  ],
);
ob_start(); ?>
  <p class="_emphasis">
    SCG joins Mitsui & Co. and Credit Saison
    Establishing “Siam Saison” to provide financial service
    For construction materials dealers and contractors in Thailand
  </p>
  <p>
    <b>Bangkok, 5 November 2018:</b>  SCG Trading, a subsidiary of SCG, has today signed an agreement with Mitsui & Co. and Credit Saison Co., Ltd. to establish a new
    joint venture company <b>“Siam Saison Co., Ltd.”</b> to provide B2B financial service
    for construction materials dealers and contractors in Thailand.
  </p>
  <p>
    Mr. Bunn Kasemsup (middle), Managing Director of SCG Trading Co., Ltd.,
    Mr. Yoshihiro Iwata (left), General Manager of Internet Service Business Division, Mitsui & Co., and Mr. Katsumi Mizuno (right), Managing Director and Head of Global Business Division, Credit Saison Co., Ltd. have recently signed an agreement together at SCG Headquarters in Bangkok.
  </p>
  <p>
    Siam Saison Co., Ltd. will offer wide range of B2B financial services targeting on construction materials dealers and contractors in the first phase. Financial supports will enhance business capability and increase competitive advantage of construction materials dealers. The company also plans to expand its business to reach additional target groups.
  </p>
  <p>
    SCG Trading will hold direct stake of 30.6%, while Mitsui & Co. and Credit Saison will hold 29.4% and 40.0% stake respectively.  The operations are expected to commence within the first quarter of 2019.
  </p>
  <p>
    Wholly-owned and established by SCG since 1978, SCG Trading has full access to the expertise and resources of the leading powerful conglomerate to enhance its import and export business since its establishment. In addition, SCG Trading also cooperates with leading international companies to explore new business opportunities both in Thailand and in other countries.
  </p>
  <p>
    Established since 1957, Mitsui & Co. is one of the largest global trading companies with business area in product sales, worldwide logistics and financing. Mitsui & Co. develops its business in Thailand in various areas cooperating with leading local business enterprises including SCG, and has abundant business know-how in Thailand.
  </p>
  <p>
    Credit Saison Co., Ltd., established in 1951, is a Japanese financial services company affiliated to Mizuho Financial Group.  Credit Saison provides credit, lease, finance, and real estate in Japan and internationally. The company offers credit and prepaid cards; loan collection agency services; and corporate cards for companies and individual proprietor.
  </p>
<?php $data[] = array(
  "title"     => "SCG Joins Mitsui & Co. and credit saison",
  "category"  => "activity",
  "posted_at" => "2017-12-19 14:00:07",
  "content"   => ob_get_clean(),
  "thumb"     => "./img/news/news-2.jpg",
  "images"    => [
    "./img/news/news-2.jpg",
  ],
);
ob_start(); ?>
  <p>
    SCG hosts SCG TOP DEALER AWARD 2017 at the Convention Hall of Centara Grand at Central World, gratitude in the dedication , determination and collaboration of all dealers of SCG’s construction materials both inside and outside of ASEAN throughout 2017 . The event welcomes over 300 attendees with the theme “Flying Beyond Tomorrow” and a “swan” symbolising success and the future of greater prosperity and strength
  </p>
<?php $data[] = array(
  "title"     => "SCG TOP dealer award the awarding ceremony the great success of overseas construction material dealers",
  "category"  => "activity",
  "posted_at" => "2017-12-19 14:00:07",
  "content"   => ob_get_clean(),
  "thumb"     => "./img/news/news-3.jpg",
  "images"    => [
    "./img/news/news-3.jpg",
    "./img/news/content/news3-0.jpg",
    "./img/news/content/news3-1.jpg",
    "./img/news/content/news3-2.jpg",
    "./img/news/content/news3-3.jpg",
    "./img/news/content/news3-4.jpg",
    "./img/news/content/news3-5.jpg",
    "./img/news/content/news3-6.jpg",
    "./img/news/content/news3-7.jpg",
  ],
);
ob_start(); ?>
  <p>
    SCG Trading Co.,Ltd.,  a subsidiary of SCG, has recently signed an MOU with College of Arts, Media, and Technology of Chiang Mai University on “Cross Border e-Commerce” project.
    This collaboration aims to explore new business opportunity and support Thai SMEs in marketing Thai products to Chinese customers via e-Commerce channel, using the strength of both organizations. Many Thai products are well known by Chinese customers including food and cosmetics. In addition, Chiang Mai Province is one of the key busimess area of Northern Thailand and very famous destination of Chinese tourists every year.
  </p>
<?php $data[] = array(
  "title"     => "SCG TRADING AND CHIANG MAI UNIVERSITY 
SIGNED MOU FOR THE DEVELOPMENT OF CROSS BORDER E-COMMERCE",
  "category"  => "activity",
  "posted_at" => "2017-12-19 14:00:07",
  "content"   => ob_get_clean(),
  "thumb"     => "./img/news/content/news1-1.jpg",
  "images"    => [
    "./img/news/content/news1-1.jpg",
    "./img/news/content/news1-2.jpg",
    "./img/news/content/news1-3.jpg",
  ],
);

if(isset($_GET['index'])) {
  echo json_encode($data[$_GET['index']]);
} else {
  echo json_encode($data);
}

