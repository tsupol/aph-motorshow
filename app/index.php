<?php
require_once('header.php');
get_header('page-home');
?>
  <div class="bg-wrap">
    <div class="layout-outer theme-dark">
      <div class="layout-inner">
        <h1 class="_banner-title-1">Motor Show <span class="color-1">2019</span></h1>
        <div class="_banner-title-2">IMPACT เมืองทองธานี อาคารชาลเลนเจอร์ 1-3</div>
        <div class="_banner-title-3">27 มีนาคม - 7 เมษายน 2562</div>
        <div class="_banner-title-4">จ. - ศ. 12:00 - 22:00 | ส. - อา. 11:00 - 22:00</div>
      </div><!-- .layout-inner -->
    </div><!-- .layout-outer -->

    <div class="layout-full-width">
      <div class="sec-bottom">
        <div class="flip-link-container">

          <div class="flip-link">
            <div class=flip-img-wrap>
<!--              <img class="flip-img __honda-bike" src="--><?php //echo $asset_path . '/img/home-honda-bike.jpg' ?><!--"/>-->
              <img class="flip-img __honda-bike" src="http://hondamotorshow.democnc.com/app/themes/hondamotorshow/assets/img/over/card-ap.png"/>
            </div>
          </div>

          <div class="flip-link">
            <div class=flip-img-wrap>
<!--              <img class="flip-img __cub-house" src="--><?php //echo $asset_path . '/img/home-cub-house.jpg' ?><!--"/>-->
              <img class="flip-img __cub-house" src="http://hondamotorshow.democnc.com/app/themes/hondamotorshow/assets/img/over/card-cub.jpg"/>
            </div>
          </div>

        </div>

        <div class="btn-container">
          <div class="btn-wrap">
            <a href="./page-aphonda.php" class="ts-btn">
              <span>Honda Bike</span>
            </a>
          </div>
          <div class="btn-wrap theme-cub">
            <a href="./page-cub-house.php" class="ts-btn"><span>CUB House</span></a>
          </div>
        </div>
        
      </div>
    </div>
  </div>
<?php
include_once('footer.php');
