<?php
require_once('header.php');
get_header('page-cub-house');
?>

  <div class="layout-outer theme-cub">

    <!-- The Banner Background -->
    <div class="banner-bg-wrap"><div class="banner-bg"></div></div>


    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
      </div>

      <!-- The Banner -->
      <div class="banner-page">
        <h1>CUB House</h1>
        <h2>Motor Show <span class="color-primary">2019</span></h2>
        <a href="#" class="ts-btn btn-download">
          <span class="btn-title">ดาวน์โหลดโบรชัวร์</span>
          <span class="btn-subtitle">pdf 3.5 MB</span>
        </a>
      </div>

      <!-- The Content -->
      <div class="section-wrap">
        <div class="highlight-wrap">
          <div class="title-wrap">
            <h1 class="section-title color-primary">HIGHLIGHT</h1>
            <h2 class="section-subtitle">รวมความพิเศษที่จะเกิดขึ้นในงาน</h2>
          </div>

          <div class="carousel-container-wrap">
            <div class="carousel-container">
              <div class="carousel-video-wrap">
                <div class="carousel-wrap carousel-video">
                  <!-- slide 1 Video -->
                  <div class="carousel-item">
                    <div class="jetpack-video-wrapper">
                      <iframe src="https://www.youtube.com/embed/Y_CvCUeJQqA?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen="" data-crossorigin="y" data-ratio="0.5625" data-width="640" data-height="360"
                              style="display: block; margin: 0px; width: 613px; height: 344.813px;"></iframe>
                    </div>
                  </div>
                  <!-- slide 2 -->
                  <a href="#" class="carousel-item with-bottom-gradient">
                    <div class="carousel-img"
                         style="background-image: url(<?php echo get_template_directory_uri() . '/img/placeholder/ph-lg-1.jpg' ?>"></div>
                    <div class="_text-wrap">
                      <div class="carousel-date">23 มี.ค. 2562</div>
                      <div class="carousel-title">
                        Honda เปิดตัวมอเตอร์ไซค์ใหม่ 4 รุ่น<br/>
                        รับปีหมูทอง 2019
                      </div>
                    </div>
                  </a>

                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="card-grid grid-50">
          <?php for ($i = 0; $i < 6; $i++): ?>
            <div class="card-col md-col-12">
              <a href="#" class="section-card"
                 style="background-image: url(<?php echo get_template_directory_uri() . 'img/placeholder/section-bg.jpg' ?>)">
                <div class="text-wrap">
                  <div class="section-card-title">Models</div>
                  <div class="section-card-subtitle">รวมรถรุ่นใหม่ที่จะแสดงในงาน</div>
                </div>
              </a>
            </div>
          <?php endfor; ?>
        </div>
      </div>

      <div class="section-wrap">
        <a href="./page-news-video.php">
          <h1 class="section-title color-primary">News & Video</h1>
          <h2 class="section-subtitle">ข่าวสารและกิจกรรมต่างๆที่เดิดขึ้นในงาน</h2>
          <span class="_chevron"></span>
        </a>
        <div class="card-grid grid-33">
          <?php for ($i = 0; $i < 3; $i++): ?>
            <div class="card-col">
              <a href="./page-content.php" class="news-card">
                <div class="_inner">
                  <div class="news-date">25 มี.ค. 2562</div>
                  <div class="news-thumb-wrap">
                    <img class="news-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/ph-lg-1.jpg' ?>"/>
                  </div>
                  <div class="news-title">เปิดตัว CUB House โชว์รูมวางจำหน่ายรถมอเตอร์ไซค์ Honda รุ่นพิเศษที่มีตำนาน</div>
                </div>
              </a>
            </div>
          <?php endfor; ?>
        </div>
      </div>

    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
