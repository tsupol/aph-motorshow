<?php
require_once('header.php');
get_header('page-news-video');
?>
  <div class="layout-outer theme-light">
    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">News & Video</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title">News & Video</h1>

      <!-- The Content -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 12; $i++): ?>
          <div class="card-col">
            <a href="./page-content.php" class="news-card">
              <div class="_inner">
                <div class="news-date">25 มี.ค. 2562</div>
                <div class="news-thumb-wrap">
                  <img class="news-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/ph-lg-1.jpg' ?>"/>
                </div>
                <div class="news-title">เปิดตัว CUB House โชว์รูมวางจำหน่ายรถมอเตอร์ไซค์ Honda รุ่นพิเศษที่มีตำนาน</div>
              </div>
            </a>
          </div>
        <?php endfor; ?>
      </div>

      <?php get_template_part('template-parts/aph/pagination') ?>

    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
