<?php
?>
</div> <!-- #footer-bottom-push -->
<!-- Footer -->
<div id="colophon" class="site-footer">
  <div class="footer-top">
    <div class="footer-inner">
      <!-- Logo -->
      <div class="brand-wrap flex-center">
        <?php the_custom_logo() ?>
        <div class="_title">
          A.P. Honda Motor Show 2019
        </div>
      </div>

      <!-- Content -->
      <ul class="footer-content">
        <?php get_template_part('template-parts/aph/menu-content-primary') ?>
        <li class="menu-item menu-item-has-children">
          <a>(hidden)</a>
          <ul class="sub-menu">
            <li class="menu-item"><a href="./page-introduction.php">รถจักรยานยนต์ฮอนด้า</a></li>
            <li class="menu-item"><a href="./page-introduction.php">บิ๊กไบค์ฮอนด้า</a></li>
            <li class="menu-item"><a href="./page-introduction.php">คับเฮาส์ฮอนด้า</a></li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
  <div class="footer-bottom">
    <div class="footer-inner">
      <div class="footer-copyright">COPYRIGHT © 2019 A.P. HONDA CO.,LTD. ALL RIGHTS RESERVED</div>
      <!-- Social (Same structure as in primary-navigation) -->
      <?php get_template_part('template-parts/aph/menu-content-social') ?>
    </div>
  </div>
</div>

</div><!-- .layout-main -->

<?php include "./popups/popup-contact.php" ?>
<?php include "./popups/popup-image.php" ?>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe-ui-default.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/macy@2.4.0/dist/macy.min.js"></script>

<script src="./dist/app.bundle.js?<?php echo time(); ?>"></script>

</body>
</html>
