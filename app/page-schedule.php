<?php
require_once('header.php');
get_header('page-schedule');

function getSubContent() {
  ?>
  <ul class="sub-content"> <!-- sub -->
    <li class="sub-item">
      <a class="acc-label"><div class="_time">09:59 - 10.30</div><div class="_desc">มีการแสดงเปิดบูท</div></a>
      <a class="acc-label"><div class="_time">10:30 - 11.30</div><div class="_desc">เปิดตัวรถรุ่นใหม่</div></a>
      <a class="acc-label"><div class="_time">14:30 - 15:30</div><div class="_desc">แชร์ประสบการณ์กับนักแข่งชื่อดัง</div></a>
      <a class="acc-label"><div class="_time">15:00 - 16.30</div><div class="_desc">พูดคุยเรื่องการขับขี่</div></a>
    </li>
  </ul>
  <?php
}
?>
  <div class="layout-outer">
    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Time Schedule</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title margin-bottom-0">Time Schedule</h1>

      <!-- The Content -->
      <ul class="ts-accordion">

        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label color-fade">วันจันทร์ที่ 25 มีนาคม 2562 : รอบวีไอพี</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label color-fade">วันจันทร์ที่ 25 มีนาคม 2562 : รอบวีไอพี</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label">วันพุธที่ 27 มีนาคม 2562 : รอบบุคคลทั่วไป</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label">วันพฤหัสบดีที่ 28 มีนาคม 2562</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label">วันศุกร์ที่ 29 มีนาคม 2562</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label color-1">วันเสาร์ที่ 30 มีนาคม 2562</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label color-1">วันอาทิตย์ีที่ 31 มีนาคม 2562</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label">วันจันทร์ที่ 1 เมษายน 2562</a>
          <?php getSubContent() ?>
        </li>
        <li class="acc-item has-children"> <!-- main -->
          <a class="acc-label">วันอังคารที่ 2 เมษายน 2562</a>
          <?php getSubContent() ?>
        </li>

      </ul>

    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
