<?php
require_once('header.php');
get_header('page-test'); // using the same body's class as Wordpress' post
?>

<div id="ts-alert-container">

<!---->
<!--  <div class="ts-alert">-->
<!--    <img class="alert-icon" src="--><?php //echo $asset_path . '/img/icon-alert.png' ?><!--"/>-->
<!--    <div class="alert-text">-->
<!--      <div class="text-inner">-->
<!--        <span class="alert-time">09:59 - 10.30</span>-->
<!--        <span>A.P. Honda มีการแสดงเปิดบูท</span>-->
<!--      </div>-->
<!--    </div>-->
<!--    <div class="alert-close">-->
<!--      <div class="close-inner"></div>-->
<!--    </div>-->
<!--  </div>-->

</div>

<div class="layout-outer">
  <div class="layout-inner page-wrapper">
    <button id="show-alert">Show Alert</button>
    <button id="show-alert-cub">Show Alert CUB</button>
    <button id="show-notification">Show Notification</button>
  </div>
</div>

<?php include_once('footer.php') ?>
