<?php
require_once('header.php');
/**
 * Note: Switch the page to launched here!
 */
get_header('single-post'); // launched
// get_header('single-post page-pre-launch'); // pre-launch
?>

<main id="main" class="site-main">
  <div class="layout-outer theme-light">
    <!--  <div class="layout-outer">-->
    <div class="layout-inner">

      <article class="post type-post has-post-thumbnail">

        <header class="entry-header">

          <a href="#" class="entry-back"><span>Back</span></a>
          <div class="entry-crumbs">
            <span><a class="entry-crumb" href="#">Home</a></span>
            <span class="_gt">&gt;</span>
            <span><a class="entry-crumb" href="#">Honda Bike</a></span>
            <span class="_gt">&gt;</span>
            <span><a class="entry-crumb" href="#">News & Video</a></span>
            <span class="_gt">&gt;</span>
            <span><a class="entry-crumb" href="#">Honda เปิดตัวมอเตอร์ไซค์ใหม่ 2 รุ่นรับปีหมูทอง 2019</a></span>
          </div>
          <h1 class="entry-title">Honda เปิดตัวมอเตอร์ไซค์ใหม่ 2 รุ่นรับปีหมูทอง 2019</h1>
          <div class="entry-meta-date"><a href="#">26 มี.ค. 2562</a></div>
          <div class="entry-shares">
            <div class="_label">Share on :</div>
            <a href="https://www.facebook.com/sharer/sharer.php?[the url here]" target="_blank"><img class="_icon" src="<?php echo $asset_path . '/img/icon-facebook-invert.png' ?>"/></a>
            <a href="https://lineit.line.me/share/ui?url=[the url here]" target="_blank"><img class="_icon" src="<?php echo $asset_path . '/img/icon-line-invert.png' ?>"/></a>
            <a href="http://www.twitter.com/share?url=[the url here]" target="_blank"><img class="_icon" src="<?php echo $asset_path . '/img/icon-twitter-invert.png' ?>"/></a>
          </div>
          <div class="entry-meta"></div>

        </header><!-- .entry-header -->

        <div class="post-thumbnail">
          <img class="wp-post-image" src="<?php echo get_template_directory_uri() . 'img/placeholder/ph-lg-1.jpg' ?>"/>
        </div>

        <div class="entry-content">

<!--          <figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">-->
<!--            <div class="wp-block-embed__wrapper">-->
<!--              <div class="jetpack-video-wrapper">-->
<!--                <iframe src="https://www.youtube.com/embed/Y_CvCUeJQqA?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"-->
<!--                        allowfullscreen="" data-crossorigin="y" data-ratio="0.5625" data-width="640" data-height="360" style="display: block; margin: 0px; width: 613px; height: 344.813px;"></iframe>-->
<!--              </div>-->
<!--            </div>-->
<!--          </figure>-->

          <h4>
            เอ.พี. ฮอนด้า ประกาศความสำเร็จในปี 2561 ด้วยยอดขาย 1,404,000 คัน ครองส่วนแบ่งการตลาด 78.5% เป็นผู้นำตลาดรถจักรยานยนต์ไทยต่อเนื่องเป็นปีที่ 30 เดินหน้ารับศักราชใหม่ภายใต้กลยุทธ์แบรนด์ “WHAT
            STOPS YOU? มุ่งไป อย่าให้อะไรมาหยุด” ด้วยแผนการตลาดเชิงรุก Insight Centric ที่เข้าถึงและเข้าใจผู้บริโภคในยุคดิจิตอล พร้อมนำเสนอผลิตภัณฑ์หลากหลาย เพียบพร้อมเทคโนโลยียานยนต์ทันสมัย
            เพื่อมอบความสุขให้แก่ผู้ใช้รถจักรยานยนต์ฮอนด้า ประเดิมปีหมูทองด้วยรถใหม่ 4 โมเดล จำหน่ายพร้อมกันทั่วประเทศในเดือนมกราคมนี้
          </h4>

          <p>
            มร. โยอิจิ มิซึทานิ ประธานกรรมการบริหาร บริษัท เอ.พี. ฮอนด้า จำกัด ผู้จัดจำหน่ายรถจักรยานยนต์ฮอนด้าในประเทศไทย เปิดเผยว่า “ปี 2561 ที่ผ่านมาประเทศไทยมีอัตราเติบโตของ GDP ที่ 4%
            แต่ตลาดรถจักรยานยนต์ไม่ได้เติบโตสอดคล้องกับ GDP ด้วยสภาพเศรษฐกิจทั้งภายนอกและภายในประเทศที่มีความผันผวนตลอดทั้งปี ส่งผลให้ตลาดรวมรถจักรยานยนต์ปี
          </p>

          <figure class="wp-block-image">
            <img src="<?php echo get_template_directory_uri() . 'img/placeholder/ph-lg-1.jpg' ?>"/>
          </figure>

          <p>เรามองว่าตลาดรถจักรยานยนต์ไทยในปีนี้ จะอยู่ที่ 1.72 ล้านคัน และ Honda ตั้งเป้าการจำหน่ายที่ 1.36 ล้านคัน ส่วนตลาดรถ Big Bike ปี 2561 ปิดตลาดที่ 31,179 คัน เติบโตขึ้น 5%
            ฮอนด้ามียอดจดทะเบียน 13,285 คัน เติบโตขึ้น 3% คิดเป็นส่วนแบ่งการตลาดที่ 43% ครองตลาดเป็นอันดับ 1 ติดต่อกันเป็นปีที่ 5 โดยในปี 2562 นี้คาดว่าตลาดรถ Big Bike จะมีขนาดใกล้เคียงปีที่ผ่านมาที่
            31,500 คัน โดยฮอนด้าตั้งเป้าการจำหน่ายที่ 13,300 คัน” ส่วนของคับเฮ้าส์ (CUB House) ธุรกิจใหม่ที่เราริเริ่มขึ้นในปีที่ผ่านมา
            ได้สร้างให้เกิดวัฒนธรรมในการขับขี่รถจักรยานยนต์รูปแบบไลฟ์สไตล์ใหม่ ๆ เกิดขึ้น โดยล่าสุดมีเน็ตเวิร์ค CUB House ตามจังหวัดต่าง ๆ 8 แห่งทั่วประเทศแล้ว”
          </p>

          <figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
            <div class="wp-block-embed__wrapper">
              <div class="jetpack-video-wrapper">
                <iframe src="https://www.youtube.com/embed/Y_CvCUeJQqA?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="" data-crossorigin="y" data-ratio="0.5625" data-width="640" data-height="360" style="display: block; margin: 0px; width: 613px; height: 344.813px;"></iframe>
              </div>
            </div>
          </figure>

          <p>เรามองว่าตลาดรถจักรยานยนต์ไทยในปีนี้ จะอยู่ที่ 1.72 ล้านคัน และ Honda ตั้งเป้าการจำหน่ายที่ 1.36 ล้านคัน ส่วนตลาดรถ Big Bike ปี 2561 ปิดตลาดที่ 31,179 คัน เติบโตขึ้น 5%
            ฮอนด้ามียอดจดทะเบียน 13,285 คัน เติบโตขึ้น 3% คิดเป็นส่วนแบ่งการตลาดที่ 43% ครองตลาดเป็นอันดับ 1 ติดต่อกันเป็นปีที่ 5 โดยในปี 2562 นี้คาดว่าตลาดรถ Big Bike จะมีขนาดใกล้เคียงปีที่ผ่านมาที่
            31,500 คัน โดยฮอนด้าตั้งเป้าการจำหน่ายที่ 13,300 คัน” ส่วนของคับเฮ้าส์ (CUB House) ธุรกิจใหม่ที่เราริเริ่มขึ้นในปีที่ผ่านมา
            ได้สร้างให้เกิดวัฒนธรรมในการขับขี่รถจักรยานยนต์รูปแบบไลฟ์สไตล์ใหม่ ๆ เกิดขึ้น โดยล่าสุดมีเน็ตเวิร์ค CUB House ตามจังหวัดต่าง ๆ 8 แห่งทั่วประเทศแล้ว”
          </p>

          <div class="post-thumbnail">
            <figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
              <div class="wp-block-embed__wrapper">
                <div class="jetpack-video-wrapper">
                  <iframe src="https://www.youtube.com/embed/Y_CvCUeJQqA?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                          allowfullscreen="" data-crossorigin="y" data-ratio="0.5625" data-width="640" data-height="360" style="display: block; margin: 0px; width: 613px; height: 344.813px;"></iframe>
                </div>
              </div>
            </figure>
          </div>

          <p>เรามองว่าตลาดรถจักรยานยนต์ไทยในปีนี้ จะอยู่ที่ 1.72 ล้านคัน และ Honda ตั้งเป้าการจำหน่ายที่ 1.36 ล้านคัน ส่วนตลาดรถ Big Bike ปี 2561 ปิดตลาดที่ 31,179 คัน เติบโตขึ้น 5%
            ฮอนด้ามียอดจดทะเบียน 13,285 คัน เติบโตขึ้น 3% คิดเป็นส่วนแบ่งการตลาดที่ 43% ครองตลาดเป็นอันดับ 1 ติดต่อกันเป็นปีที่ 5 โดยในปี 2562 นี้คาดว่าตลาดรถ Big Bike จะมีขนาดใกล้เคียงปีที่ผ่านมาที่
            31,500 คัน โดยฮอนด้าตั้งเป้าการจำหน่ายที่ 13,300 คัน” ส่วนของคับเฮ้าส์ (CUB House) ธุรกิจใหม่ที่เราริเริ่มขึ้นในปีที่ผ่านมา
            ได้สร้างให้เกิดวัฒนธรรมในการขับขี่รถจักรยานยนต์รูปแบบไลฟ์สไตล์ใหม่ ๆ เกิดขึ้น โดยล่าสุดมีเน็ตเวิร์ค CUB House ตามจังหวัดต่าง ๆ 8 แห่งทั่วประเทศแล้ว”
          </p>


        </div><!-- .entry-content -->

      </article>

      <!-- Widget - Related Models -->
      <div class="widget-related-model card-container">
        <div class="card-title">
          <span>Model ที่เกี่ยวข้อง</span>
        </div>
        <div class="card-grid">
          <?php for ($i = 0; $i < 2; $i++): ?>
            <a href="#" class="product-card card-col">
              <div class="_inner">
                <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
                <div class="inside-label">C125</div>
              </div>
            </a>
          <?php endfor; ?>
        </div>
      </div>

      <!-- Widget - Related News -->
      <div class="widget-related-news card-container">
        <div class="card-title">
          <span>ข่าวที่เกี่ยวข้อง</span>
        </div>
        <div class="card-grid">
          <?php for ($i = 0; $i < 4; $i++): ?>
            <a href="#" class="news-card card-col">
              <div class="_inner">
                <div class="news-date">25 มี.ค. 2562</div>
                <div class="news-thumb-wrap">
                  <img class="news-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/ph-lg-1.jpg' ?>"/>
                </div>
                <div class="news-title">เปิดตัว CUB House โชว์รูมวางจำหน่ายรถมอเตอร์ไซค์ Honda รุ่นพิเศษที่มีตำนาน</div>
              </div>
            </a>
          <?php endfor; ?>
        </div>
      </div>


    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
</main>

<?php include_once('footer.php'); ?>
