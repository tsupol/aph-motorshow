<!-- Popup -->
<div id="popup-image" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">

      <div class="sp-close close-popup"></div>

      <img src="<?php echo get_template_directory_uri() . 'img/placeholder/banner-single-model.jpg' ?>"/>

    </div>
  </div>

</div>
