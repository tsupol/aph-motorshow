<?php
require_once('header.php');
get_header('page-countdown-2 page-pre-launch')
?>

<?php get_template_part('template-parts/aph/section-countdown') ?>

<?php
$carousel_data = array(
  array(
    'image' => '/img/placeholder/ph-lg-1.jpg',
    'date'  => '22 มี.ค. 2562',
  ),
  array(
    'image' => '/img/placeholder/ph-lg-2.jpg',
    'date'  => '23 มี.ค. 2562',
  ),
)
?>

<div class="layout-outer sec-countdown-news theme-dark">
  <div class="layout-inner">
    <h2>UPDATED NEWS</h2>
    <h3>ข่าวสารใหม่ๆที่เกิดขึ้น</h3>
    <div class="right-wrap">
      <div class="carousel-container">
        <div class="carousel-wrap">
          <?php foreach ($carousel_data as $data) : ?>
            <a href="#" class="carousel-item with-bottom-gradient">
              <div class="carousel-img" style="background-image: url('<?php echo get_template_directory_uri() . $data['image'] ?>')"></div>
              <div class="_text-wrap">
                <div class="carousel-date"><?php echo $data['date'] ?></div>
                <div class="carousel-title">
                  Honda เปิดตัวมอเตอร์ไซค์ใหม่ 4 รุ่น<br/>
                  รับปีหมูทอง 2019
                </div>
              </div>
            </a>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <div class="side-news-wrap">
      <a href="#" class="news-card">
        <div class="news-date">23 มี.ค. 2562</div>
        <div class="news-thumb-wrap">
          <img class="news-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/home-thumb-1.jpg' ?>"/>
        </div>
        <div class="news-title">เปิดตัว CUB House โชว์รูมวางจำหน่ายรถมอเตอร์ไซค์ Honda รุ่นพิเศษที่มีตำนาน</div>
      </a>

      <a href="#" class="news-card">
        <div class="news-date">23 มี.ค. 2562</div>
        <div class="news-thumb-wrap">
          <img class="news-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/home-thumb-2.jpg' ?>"/>
        </div>
        <div class="news-title">เผยโฉม Honda Super Cub C125 ถ้ารู้จักจะรัก Cub</div>
      </a>

    </div>
  </div>
</div>

<div class="layout-outer sec-countdown-links theme-dark">
  <div class="layout-inner">
    <div class="links-wrap">
      <a href="#">
        <img src="<?php echo get_template_directory_uri() . 'img/logo/logo-honda-red.jpg' ?>"/>
      </a>
      <a href="#">
        <img src="<?php echo get_template_directory_uri() . 'img/logo/logo-honda-black.jpg' ?>"/>
      </a>
      <a href="#">
        <img src="<?php echo get_template_directory_uri() . 'img/logo/logo-cub.jpg' ?>"/>
      </a>
    </div>
  </div>
</div>



<?php include_once('footer.php') ?>
