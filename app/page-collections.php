<?php
require_once('header.php');
get_header('page-collections');
?>

  <div class="layout-outer">

    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Collection</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title-2">Collection</h1>

      <!-- The Tab -->
      <ul class="ts-tabs tab-3">
        <li class="tab-item"><a data-content-selector="#tab-content-1"><span>Marc Márquez</span></a></li>
        <li class="tab-item active"><a data-content-selector="#tab-content-2"><span>Kushitani</span></a></li>
        <li class="tab-item"><a data-content-selector="#tab-content-3"><span>Honda Collection</span></a></li>
      </ul>

      <!-- Content for tab 1 -->
      <?php for ($g = 0; $g < 3; $g++): ?>
        <div class="tab-content" id="tab-content-<?php echo $g + 1 ?>">
          <!-- Grid -->
          <div class="card-grid category-content-<?php echo $g + 1 ?>">
            <?php for ($i = 0; $i < $g + 6; $i++): ?>
              <a href="#" class="product-card card-col">
                <div class="_inner">
                  <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
                  <div class="inside-label">หมวกกันน็อกเบลล์ คัทตอม500</div>
                </div>
                <div class="product-price">
                  ราคา <span class="color-primary">700</span> บาท
                </div>
              </a>
            <?php endfor; ?>
          </div>
        </div>
      <?php endfor; ?>


    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
