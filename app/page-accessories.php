<?php
require_once('header.php');
get_header('page-accessories');
?>

  <div class="layout-outer">

    <!-- The Banner Background -->
    <div class="banner-bg-wrap">
      <div class="banner-bg"></div>
    </div>

    <div class="layout-inner page-wrapper">

      <!-- The Bread Crumbs -->
      <div class="entry-crumbs">
        <span><a class="entry-crumb" href="#">Home</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Honda Bike</a></span>
        <span class="_gt">&gt;</span>
        <span><a class="entry-crumb" href="#">Accessories</a></span>
      </div>

      <!-- The Title -->
      <h1 class="main-title-2">Accessories</h1>

      <!-- The Tab -->
      <ul class="ts-tabs">
        <li class="tab-item"><a href="#"><span>C125</span></a></li>
        <li class="tab-item active "><a href="#"><span>Monkey</span></a></li>
      </ul>

      <!-- Grid -->
      <div class="card-grid">
        <?php for ($i = 0; $i < 3; $i++): ?>
          <a href="#" class="product-card card-col">
            <div class="_inner">
              <img class="product-thumb" src="<?php echo get_template_directory_uri() . 'img/placeholder/product-1.jpg' ?>"/>
              <div class="inside-label">หมวกกันน็อกเบลล์ คัทตอม500</div>
            </div>
            <div class="product-price">
              ราคา <span class="color-primary">700</span> บาท
            </div>
          </a>
        <?php endfor; ?>
      </div>


    </div><!-- .layout-inner -->
  </div><!-- .layout-outer -->
<?php
include_once('footer.php');
