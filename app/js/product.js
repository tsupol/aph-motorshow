(function ($) {
  $(document).ready(function () {

    // Tab mechanism
    // ----------------------------------------

    var $tabs = $('.ts-tabs');
    var $contents = $('.tab-content');
    $('.ts-tabs a').click(function (e) {
      e.preventDefault();
      var me = $(this);
      var selector = me.data('content-selector');
      $tabs.find('.active').removeClass('active');
      $contents.removeClass('active');
      $(selector).addClass('active');
      me.parent().addClass('active');
    });
    // open currently active tab
    $tabs.find('.active a').trigger( "click" );


    // Category mechanism (Only works when in .tab-content)
    // ----------------------------------------

    $('.btn-toggle a').click(function (e) {
      e.preventDefault();
      var me = $(this);
      var $container = me.parents('.tab-content').first();
      var selector = me.data('content-selector');
      $container.find('.active').removeClass('active');
      $container.find(selector).addClass('active'); // target
      me.parent().addClass('active'); // button
    });
    // open currently active category
    $('.btn-toggle.active a').trigger( "click" );


    // Color Switcher
    // ----------------------------------------

    var $colors = $('.model-colors');
    var $imgs = $('.model-img-wrap');
    var $label = $('.model-color-label');
    $colors.children('.model-color').click(function (e) {
      e.preventDefault();
      var me = $(this);
      var selector = me.data('content-selector');
      var textColor = me.data('text-color');
      var textLabel = me.data('text-label');
      $colors.find('.active').removeClass('active');
      $imgs.find('.active').removeClass('active');
      $(selector).addClass('active');
      me.addClass('active');
      $label.css({
        'color': textColor,
      }).html(textLabel || '-')
    })

  });
})(jQuery);
