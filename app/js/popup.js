import { disableBodyScroll, clearAllBodyScrollLocks } from './bodyScrollLock.es6'

(function ($) {

  var SCROLL_SETTINGS = {};

  // The Demo
  // ----------------------------------------

  $('#show-notification').click(function () {
    openPopupById('popup-image');
  });

  // The Mechanism
  // ----------------------------------------

  // watch
  $('._btn-model-contact').click(function (e) {
    e.preventDefault();
    openContactPopup();
    $('#popup-contact .heading1').html($(this).data('model-title'));
    $('#popup-contact .heading2').html($(this).data('model-name'));
    $('#popup-contact #model').val($(this).data('model-name'));
  });
  $('.close-popup').click(function (e) {
    e.preventDefault();
    document.body.classList.remove('body-no-scroll');
    clearAllBodyScrollLocks();
    // console.log('1', $(this).parents('.ml-popup').length);
    $(this).parents('.ml-popup').first().css({
      display: 'none',
    });
  });

  // openPopupById('popup-contact');

  function openPopupById (modalId) {
    var modal = document.getElementById(modalId);
    modal.classList.remove('email-sent');
    modal.style.display = "block";
    document.body.classList.add('body-no-scroll');
    if(modalId === 'popup-contact') {
      new PerfectScrollbar('#' + modalId + ' .scroll-container', SCROLL_SETTINGS);
    }
    disableBodyScroll(document.querySelector("#" + modalId));
  }

  function openContactPopup () {
    openPopupById('popup-contact');
  }

  // Contact form
  // ----------------------------------------

  contactFormInit();
  function contactFormInit () {
    var request;
    var validator = $('#contact-form').validate({
      rules: {
        // name: { required: true },
        // email: {
        //   required: true,
        //   email: true,
        // },
        // tel: { required: true },
      },
      messages: {
        name: { required: 'This field is required' },
        email: { required: 'This field is required' },
        tel: { required: 'This field is required' },
      },
      // send email
      submitHandler: function (form) {
        // Abort any pending request
        if (request) {
          request.abort();
        }
        // setup some local variables
        var $form = $(form);
        $('#popup-contact').addClass('sending');

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        $inputs.prop("disabled", true);

        // Fire off the request to /form.php
        request = $.ajax({
          url: "./gmail.php",
          type: "post",
          data: serializedData,
          timeout: 15000
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
          $('.ml-input').val('');
          $('#popup-contact').addClass('email-sent').removeClass('sending');
          var $scroll = $('#popup-contact .scroll-container');
          if($scroll && $scroll[0]) $scroll[0].scrollTop = 0;
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
          alert("Something went wrong. Please try again in 10 minutes.");
          // Log the error to the console
          console.error(
            "The following error occurred: " +
            textStatus, errorThrown
          );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
          // Reenable the inputs
          $inputs.prop("disabled", false);
        });
      }
    });
    $('#clear-btn').click(function () {
      $('.ml-input').val('').parent().removeClass('has-value');
      validator.resetForm();
    })
  }

})(jQuery);
