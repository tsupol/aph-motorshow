import { disableBodyScroll, clearAllBodyScrollLocks } from './bodyScrollLock.es6'

(function ($) {
// Back to top
  $('#back-to-top').click(function () {
    var duration = $(document).scrollTop() || 500;
    $('html, body').animate({ scrollTop: 0 }, { duration: parseInt(duration) / 2 });
  });

  initNavMenu();
  function initNavMenu () {

    $('#nav-toggle').click(function () {
      $('body').toggleClass('mobile-menu-expanded');
      if ($('body').hasClass('mobile-menu-expanded')) {
        disableBodyScroll(document.querySelector("#nav-expanded"));
      } else {
        clearAllBodyScrollLocks()
      }
    });
    $('#nav-expanded .nav-item, #ts-backdrop').click(function () {
      $('body').removeClass('mobile-menu-expanded');
      clearAllBodyScrollLocks()
    });

    // Nav Accordion
    // ----------------------------------------
    $('#primary-menu .menu-item-has-children, .site-footer .menu-item-has-children').click(function () {
      $(this).toggleClass('expanded')
    });

    // Component Accordion
    // ----------------------------------------
    $('.ts-accordion .has-children>.acc-label').click(function () {
      $(this).parent().toggleClass('expanded')
    });

  }

})(jQuery);
