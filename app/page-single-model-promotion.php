<?php
require_once('header.php');
get_header('page-single-model');
?>
  <main id="main" class="site-main">
    <div class="layout-outer">
      <div class="layout-inner page-wrapper">
        <article class="post type-post has-post-thumbnail">

          <header class="entry-header">
            <div class="entry-crumbs">
              <span><a class="entry-crumb" href="#">Home</a></span>
              <span class="_gt">&gt;</span>
              <span><a class="entry-crumb" href="#">Honda Bike</a></span>
              <span class="_gt">&gt;</span>
              <span><a class="entry-crumb" href="#">Models</a></span>
              <span class="_gt">&gt;</span>
              <span><a class="entry-crumb" href="#">CB150R</a></span>
            </div>

            <div class="entry-title-wrap">
              <a href="#" class="entry-back"><span>Back</span></a>
              <h1 class="entry-title">CB150R</h1>
            </div>

            <!-- Share, move to content -->
            <div class="entry-shares pinned-share">
              <div class="_label">Share on :</div>
              <a href="#"><img class="_icon" src="<?php echo $asset_path . '/img/icon-facebook-invert.png' ?>"/></a>
              <a href="#"><img class="_icon" src="<?php echo $asset_path . '/img/icon-line-invert.png' ?>"/></a>
              <a href="#"><img class="_icon" src="<?php echo $asset_path . '/img/icon-twitter-invert.png' ?>"/></a>
            </div>

            <div class="entry-meta"></div>

          </header><!-- .entry-header -->

          <div class="entry-content">

            <div class="promotions">
              <div class="promotion">
                <img class="_img" src="<?php echo $asset_path . '/img/placeholder/promotion-1.jpg' ?>"/>
                <div class="promotion-text">
                  <div>
                    เมื่อซื้อรถ CBR500R ดาวน์ 0 % ออกรถได้ทันที พร้อมรับของแถมสุดพิเศษ รวมมูลค่าสูงสุดถึง 42,000 บาท*
                    <br/>
                    <br/>
                    • <b>ฟรี</b> ประกันภัยชั้น 1<br/>
                    • <b>ฟรี</b> ทะเบียน และ พ.ร.บ.<br/>
                    • <b>ฟรี</b> Gift Voucher 10,000 บาท<br/>
                    • <b>ฟรี</b> Riding Set 11,900 บาท<br/>
                  </div>
                </div>
              </div>
            </div>

            <!-- Banner -->
            <div class="full-width-section">
              <div class="_banner-img-wrap">
                <img class="_banner-img" src="<?php echo get_template_directory_uri() . 'img/placeholder/banner-single-model.jpg' ?>"/>
              </div>
              <div class="banner-text">
                <h2>THE STREETSTER</h2>
                <h3>สายเข้ม...เต็มข้อ</h3>
                <p>เน้นความเปนคาเฟ่เพื่อดึงดูดกลุ่มลูกค้าที่เป็นวัยรุ่น เพื่อเพิ่มยอดขาและแชร์ให้มากขึ้นกว่าปัจจุบันและให้ความรู้สึกว่าเป็นรถที่หล่อ แรง และมีสไตล์</p>
              </div>
            </div>

            <div class="actual-content">
              <!-- Model Content -->
              <div class="model-content">
                <div class="model-view">
                  <div class="model-img-wrap">
                    <img class="model-img active" id="model-1" src="<?php echo get_template_directory_uri() . 'img/placeholder/model-1.jpg' ?>"/>
                    <img class="model-img" id="model-2" src="<?php echo get_template_directory_uri() . 'img/placeholder/model-1.jpg' ?>"
                         style="filter: hue-rotate(100deg)"
                    />
                    <img class="model-img" id="model-3" src="<?php echo get_template_directory_uri() . 'img/placeholder/model-1.jpg' ?>"
                         style="filter: hue-rotate(200deg)"
                    />
                    <img class="model-img" id="model-4" src="<?php echo get_template_directory_uri() . 'img/placeholder/model-1.jpg' ?>"
                         style="filter: hue-rotate(260deg)"
                    />
                  </div>
                  <div class="model-color-label">White - Black</div>
                  <div class="model-colors">
                    <div class="model-color active"
                         data-text-label="White - Black"
                         data-content-selector="#model-1"
                         style="background-image: linear-gradient(#f3f4f4, #333637);"
                    ></div>
                    <div class="model-color"
                         data-text-label="Red"
                         data-content-selector="#model-2"
                         style="background-image: linear-gradient(#fc0105, #be161c);"
                    ></div>
                    <div class="model-color"
                         data-text-label="Gray"
                         data-content-selector="#model-3"
                         style="background-image: linear-gradient(#737e70, #4d4d4c);"
                    ></div>
                    <div class="model-color"
                         data-text-label="Black"
                         data-content-selector="#model-4"
                         style="background-image: linear-gradient(#4a4b4a, #020101);"
                    ></div>
                  </div>
                  <div class="model-price-label">ราคาจำหน่ายโดยประมาณ</div>
                  <div class="model-price">
                    <span class="color-primary">643,000</span>
                    <span class="unit">บาท</span>
                  </div>
                  <div class="model-actions">
                    <a href="#" class="ts-btn btn-download btn-secondary">
                      <span class="btn-title">ดาวน์โหลดโบรชัวร์</span>
                      <span class="btn-subtitle">pdf 3.5 MB</span>
                    </a>
                    <!-- Don't forget to add the data properly -->
                    <!-- ._btn-model-contact is for opening the popup -->
                    <a href="#"
                       class="ts-btn btn-primary _btn-model-contact"
                       data-model-name="CB150R2"
                       data-model-title="สนใจรับโปรโมชัน"
                    >
                      <span>สนใจรับโปรโมชัน</span>
                    </a>
                  </div>
                </div> <!-- .model-view -->
                <div class="model-info-container">
                  <h2 class="model-info-title">
                    <span>ข้อมูลผลิตภัณฑ์</span>
                  </h2>

                  <div>
                    <div class="model-info">
                      <h3 class="model-info-label">เครื่องยนต์ :</h3>
                      <p>SC77E/Liquid-cooled 4-stroke 16-valve DOHC Inline-41,000</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ปริมาตรกระบอกสูบ (ซีซี) :</h3>
                      <p>1,000</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ความกว้างกระบอกสูบ (มม.) X ช่วงชัก (มม.) :</h3>
                      <p>76 x 55</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">อัตราส่วนแรงอัด :</h3>
                      <p>13.0:1</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบคลัทช์ :</h3>
                      <p>Wet, multiplate with diaphragm spring with assist slipper</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบส่งกำลัง :</h3>
                      <p>6-Speed manual</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบเฟืองท้าย :</h3>
                      <p>chain</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">อัตราทดรอบ/เส้นรอบวงล้อ :</h3>
                      <p>2.688</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบเกียร์ :</h3>
                      <p>1st gear 2.285 2nd gear 1.777 3rd gear 1.500 4th gear 1.333 5th gear 1.214 6th gear 1.137</p>
                    </div>
                    <div class="model-info">
                      <h3 class="model-info-label">ระบบจุดระเบิด :</h3>
                      <p>Digital transistorized with electronic advance</p>
                    </div>
                  </div>

                  <div class="bottom-btn-wrap">
                    <a href="#" class="ts-btn btn-secondary _btn-more-detail">
                      <span>ดูรายละเอียดรถเพิ่มเติม</span>
                    </a>
                    <div class="_btn-share-wrap">
                      <a href="#" class="ts-btn _btn-share"></a>
                    </div>
                  </div>


                </div> <!-- .model-info-container -->
              </div> <!-- .model-content -->
            </div>


          </div>
        </article>

      </div><!-- .layout-inner -->
    </div><!-- .layout-outer -->
  </main>
<?php
include_once('footer.php');
